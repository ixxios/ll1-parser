Require Import List.
Require Import Notations.
Require Import Logic.
Require Import Recdef.
Import ListNotations.


(* Grammar *)
Record grammar := {
  terminal : Type;
  nonterminal : Type;
  symbol := (terminal + nonterminal)%type;
  rule := (nonterminal * list symbol)%type;
  rules : list rule;
  axiom : nonterminal;
}.


(* Symbol equality *)
Definition terminal_eq_dec (G : grammar):
  forall t t' : (terminal G),
  {t = t'} + {t <> t'}.
Admitted.

Definition nonterminal_eq_dec (G : grammar):
  forall N N' : (nonterminal G),
  {N = N'} + {N <> N'}.
Admitted.

Definition symbol_eq_dec (G : grammar):
  forall N N' : (symbol G),
  {N = N'} + {N <> N'}.
Admitted.

Definition terminalbeq (G : grammar): (terminal G) -> (terminal G) -> bool.
Admitted.

Definition nonterminalbeq (G : grammar): (nonterminal G) -> (nonterminal G) -> bool.
Admitted.


(* Sentential forms and words *)
Definition word (G : grammar) := list (terminal G).
Definition sentence (G : grammar) := list (symbol G).

Fixpoint first_nonterminal_of_sentence_pos {G : grammar} (s : sentence G) (pos : nat) :=
  match s with
  | [] => None
  | x :: xs => 
    match x with
    | inr _ => Some pos
    | _ => first_nonterminal_of_sentence_pos xs (pos + 1)
    end
  end.

Fixpoint first_nonterminal_of_sentence {G : grammar} (s : sentence G) :=
  match s with
  | [] => None
  | x :: xs => 
    match x with
    | inr N => Some N
    | _ => first_nonterminal_of_sentence xs
    end
  end.

Definition split_sentence {G : grammar} (pos : nat) (s : sentence G) : option (sentence G * nonterminal G * sentence G) :=
  match (nth_error s pos) with
  | None => None
  | Some(x) => match x with
   | inr a => Some ((firstn (pos) s : sentence G),
                    (a : nonterminal G ),
                    (skipn (pos + 1) s : sentence G))
   | inl b => None
   end
  end.

Definition sentence_of_word {G : grammar} (w : word G) : sentence G :=
  List.map inl w.

Definition sentence_of_nonterminal {G : grammar} (nt : nonterminal G) : sentence G :=
  [ inr nt ].

Definition hd' {G : grammar} (l:list (terminal G)) :=
match l with
  | [] => None
  | x :: _ => Some x
end.

(* Definition of w:1 where w:k = wϵ if |w| < k or α if w = αγ and |α| = k. *)
Definition first_terminal_of_words_agree {G : grammar} (v : word G) (v' : word G) :=
  match hd' v, hd' v' with
  | None, None => True (* We let the empty list be the empty word by convention.
                          This corresponds to the case, v = v' = epsilon. *)
  | Some fv, Some fv' => terminalbeq G fv fv' = true
  | _, _ => False
  end.


(* Rules *)
Definition nonterminal_of_rule {G : grammar} (r : rule G) : nonterminal G :=
  fst r.

Definition rhs_of_rule {G : grammar} (r: rule G): sentence G :=
  snd r.

Definition apply_rule {G : grammar} (pos : nat) (r : rule G) (s : sentence G) : option (sentence G) :=
  match split_sentence pos s with
    | None => None
    | Some (prefix, nt, suffix) =>
      if (nonterminal_eq_dec G nt (nonterminal_of_rule r)) then
        Some (prefix ++ (rhs_of_rule r) ++ suffix)
      else
        None
  end.


(* Derivation *)
Inductive derivation (G : grammar) : Type :=
| EndOfDerivation
| ApplyRule : derivation G -> nat -> rule G -> derivation G.

Inductive derives (G : grammar) : (sentence G) -> (derivation G) -> (sentence G) -> Prop :=
| EmptyDerivation:
  forall s : sentence G,
  derives G s (EndOfDerivation G) s
| ApplyRuleDerivation:
  forall (s1 s2 s3 : sentence G) (d : derivation G) (pos : nat) (r : rule G),
  derives G s1 d s2 ->
  apply_rule pos r s2 = Some s3 ->
  derives G s1 (ApplyRule G d pos r) s3.

Definition recognize (G : grammar) (w : word G) :=
  exists d,
  derives G (sentence_of_nonterminal (axiom G)) d (sentence_of_word w).

Fixpoint apply_derivation {G : grammar} (s : sentence G) (d : derivation G) : option (sentence G) :=
  match d with
  | EndOfDerivation _ => Some s
  | ApplyRule _ d' pos r =>
    match apply_derivation s d' with
    | None => None
    | Some s' => apply_rule pos r s'
    end
  end.


(* Prove apply_derivation correct *)
Lemma apply_derivation_correct (G : grammar):
  forall s1 d s2,
  apply_derivation s1 d = Some s2 -> derives G s1 d s2.
Proof.
  induction d.
  - simpl.
    intros.
    inversion H.
    subst.
    apply EmptyDerivation.
  - simpl.
    destruct (apply_derivation s1 d).
    * intros.
      assert (H' : derives G s1 d s).
      apply IHd.
      reflexivity.
      apply ApplyRuleDerivation with (s2 := s) ; assumption.
    * congruence.
Qed.

Lemma apply_derivation_correct' (G : grammar):
  forall s1 d s2,
  derives G s1 d s2 -> apply_derivation s1 d = Some s2.
Proof.
  intros.
  induction H.
  * simpl.
    auto.
  * simpl.
    rewrite IHderives.
    assumption.
Qed.


(* Left-most derivation *)
Inductive left_derivation (G : grammar) : Type :=
| EndOfLeftDerivation
| ApplyRuleOnLeftNonTerminal : left_derivation G -> (rule G) -> left_derivation G.

Inductive derives_leftmost (G : grammar) : (sentence G) -> (left_derivation G) -> (sentence G) -> Prop :=
| EmptyLeftDerivation:
  forall s : sentence G,
  derives_leftmost G s (EndOfLeftDerivation G) s
| ApplyRuleLeftDerivation:
  forall (s1 s2 s3 : sentence G) (d : left_derivation G) (r : rule G) (pos : nat),
  derives_leftmost G s1 d s2 ->
  (first_nonterminal_of_sentence_pos s2 0) = Some pos ->
  apply_rule pos r s2 = Some s3 ->
  derives_leftmost G s1 (ApplyRuleOnLeftNonTerminal G d r) s3.

Definition derives_one_leftmost_derivation (G : grammar) (s1 : sentence G) (d : left_derivation G) (s2 : sentence G) :=
  forall r pos,
  derives_leftmost G s1 d s2 
  /\ (first_nonterminal_of_sentence_pos s1 0) = Some pos 
  /\ apply_rule pos r s1 = Some s2.

(* Left-recursive grammar *)
(* A grammar is left-recursive if and only if there exists
  a nonterminal symbol A that can derive
  to a sentential form with itself as the leftmost symbol. *)
Definition left_recursive (G : grammar) :=
  exists (A : nonterminal G) d (alpha : sentence G),
  let A' := sentence_of_nonterminal A in
  derives G A' d (A' ++ alpha).


(* LL(1) grammar *)
Definition LL1 (G : grammar) :=
  let A := sentence_of_nonterminal (axiom G) in
  forall
  d0 d1 d1' d2 d2'
  alpha alpha' gamma
  (N : nonterminal G)
  (u : word G) (v : word G) (v' : word G),
  let su := sentence_of_word u in
  let sv := sentence_of_word v in
  let sv' := sentence_of_word v' in
  let intermediate_sentence := (su ++ [inr N] ++ gamma) in

  ((derives_leftmost G A d0 intermediate_sentence
  (* The following derives is the application of the rule N -> alpha: *)
  /\ derives_one_leftmost_derivation G intermediate_sentence d1 (su ++ alpha ++ gamma)
  /\ derives G (su ++ alpha ++ gamma) d2 (su ++ sv))

  /\ (derives_leftmost G A d0 intermediate_sentence
  (* The following derives is the application of the rule N -> alpha': *)
  /\ derives_one_leftmost_derivation G intermediate_sentence d1' (su ++ alpha' ++ gamma)
  /\ derives G (su ++ alpha' ++ gamma) d2' (su ++ sv'))

  /\ first_terminal_of_words_agree v v') -> alpha = alpha'.


(*Lemma LL1_not_left_recursive (G : grammar):
  LL1 G -> not (left_recursive G).*)
 Lemma LL1_not_left_recursive (G : grammar):
  left_recursive G -> not (LL1 G).
Proof.
  unfold left_recursive.
  intros.
  unfold LL1.
  unfold derives_one_leftmost_derivation.
  admit.
  Admitted.
  (* Mq N -> alpha via apply_rule *)
  (* appliquer H à N, d1 et alpha=A alpha', mq ça boucle et qu'on a pas LL1 G. *)

(* LL(1) parser *)
Fixpoint LL1_parser {G : grammar}
  (predict : nonterminal G -> terminal G -> option (rule G))
  (s : sentence G) (w : word G) : bool
  :=
    match s, w with
  | [], [] => true (* case empty word *)
  | [], _ => false
  | _, [] => false
  | s :: s', a :: w' =>
    match s, a with
    | inl _, _ => LL1_parser predict s' w' (* accept *)
    | inr N, _ =>
      match predict N a with
      | None => false
      | Some (_, a'') => LL1_parser predict (a'' ++ s') w (* predict *)
      (* w isn't decreasing, we might either add a 'fuel' parameter
      or prove termination (grammar is not left-recursive or finite length of the leftmost derivation for w?) *)
      end
    end
end.

Lemma LL1_correct (G : grammar):
  let A := sentence_of_nonterminal (axiom G) in
  forall (w : word G) predict,
  LL1 G /\ recognize G w -> LL1_parser predict A w = true.
Proof.
Admitted.


Lemma LL1_correct' (G : grammar):
  let A := sentence_of_nonterminal (axiom G) in
  forall (w : word G) predict,
  LL1_parser predict A w = true -> LL1 G /\ recognize G w.
Proof.
  intros.
  destruct H.
  destruct H0.
  unfold LL1_parser.
  induction x.
Admitted.


(* Derivation tree *)
Inductive derivation_tree (G : grammar) : Type :=
| EmptyDerivationTree
| DerivationTreeNode (s : symbol G) (t : list (derivation_tree G)).


(*essai, ne construit pas bien l'arbre - peut etre vaut-il mieux partir de la racine*)
Fixpoint tree_of_derivation (G : grammar) (d: derivation G) (tree : derivation_tree G): derivation_tree G :=
  match d with
  |EndOfDerivation _ => tree
  |ApplyRule _ deriv nat rule  => let N := nonterminal_of_rule rule in
                                  let s := rhs_of_rule rule in
                                  match tree with
                                  |EmptyDerivationTree _ => let f x := DerivationTreeNode _ (x) ([EmptyDerivationTree _ ]) in
                                   DerivationTreeNode _ (inr N) (List.map f s)
                                  |DerivationTreeNode _ sy t => tree_of_derivation _ deriv (DerivationTreeNode _ (inr N) ([tree]))
                                  end
  end.

(*essai, ne fait pas bien le parcours préfixe*)
Fixpoint leftderivation_of_tree (G : grammar) (tree : derivation_tree G) (d : derivation G) (n :nat) : derivation G :=
  match tree with
  |EmptyDerivationTree _ => d
  |DerivationTreeNode _ s trees => let g x := match x with
                                              |EmptyDerivationTree _ => false
                                              |DerivationTreeNode _ _ _ => true
                                              end
                                   in
                                                
                                   let f x := match x with
                                              |EmptyDerivationTree _ => inr (axiom G) (*n'arrive jamais*)
                                              |DerivationTreeNode _ a trees => a
                                              end
                                   in
                                   let deriv := match s with
                                                |inl a => EndOfDerivation _
                                                |inr a => 
                                                 ApplyRule _ d n ( (pair a (List.map f (List.filter g trees))) : rule G)
                                                end in
                                   match trees with
                                   |[]=> deriv
                                   |t::q => leftderivation_of_tree _ (t) deriv n
                                   end
  end.
