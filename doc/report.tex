\documentclass[a4paper, 12pt, DIV=8]{scrartcl}


\usepackage[protrusion=false]{microtype}
\usepackage[french]{babel}
\usepackage[autostyle, french=guillemets]{csquotes}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=black,      
    urlcolor=blue,
}
\usepackage{enumitem}

\usepackage[backend=biber,style=numeric,autocite=plain,sorting=none]{biblatex}
\addbibresource{ll1parser.bib}

\usepackage{fontspec}
\usepackage[mathrm=sym]{unicode-math}
 \setmainfont{STIX Two Text}
 \setsansfont[
   BoldFont={Fira Sans Medium},
   ItalicFont={Fira Sans BookItalic},
   BoldItalicFont={Fira Sans Medium Italic},
   Numbers={OldStyle, Proportional},
   Scale=.9
 ]{Fira Sans Book}
 
\setmonofont[Contextuals={Alternate},Scale=.86]{Fira Code Regular}
\setmathfont{STIX Two Math}

\usepackage[cache=false]{minted}
\setminted{breaklines}

\usepackage{tikz}
\usetikzlibrary{graphdrawing.trees}



\title{Formalisation de LL(1) en Coq\\ \Large\textnormal{Travail supervisé par Yann \textsc{Régis-Gianas}}}
\author{Adrienne \textsc{Lancelot} \and Julien \textsc{Coolen}}
\date{Année 2020}


\usepackage[amsmath, thmmarks]{ntheorem}

\theoremstyle{break}
 \theoremheaderfont{\sffamily\bfseries}
 \theorembodyfont{\normalfont}
 \theoremseparator{.\,}
\newtheorem{definition}{Définition}[section]
\newtheorem{theorem}{Théorème}
\newtheorem{corollary}{Corollaire}[theorem]
\newtheorem{lemma}[theorem]{Lemme}
\newtheorem{invariant}[theorem]{Invariant}
%\theoremstyle{plain}
\newtheorem{remark}{Remarque}
\newtheorem{example}{Exemple}

\theoremstyle{nonumberplain}
\newtheorem{notation}{Notations}
 \theoremheaderfont{\sffamily\itshape}
 \theorembodyfont{\normalfont}
 \theoremseparator{.\,}
 %\theoremsymbol{\ensuremath{\checkmark}}
 \newtheorem{proof}{Preuve}
 

\newcommand{\fst}{\textnormal{FIRST}}
\newcommand{\follow}{\textnormal{FOLLOW}}
\newcommand{\Nat}{\textnormal{\textbf{N}}}

\begin{document}
\maketitle
\tableofcontents

\newpage

\section{Contexte}

Les étapes de la compilation d'un programme :

Programme sous forme textuelle \(\longrightarrow\) Reconnaissance des lexèmes (ou \textit{tokens}, via l'analyse lexicale) \(\longrightarrow\) Le programme appartient-il au langage ? Si oui, production d'un arbre de syntaxe abstraite (via l'analyse syntaxique).

On peut aussi ajouter une autre étape, l'analyse sémantique. Cette étape consiste typiquement à vérifier que les variables sont initialisées ou que les expressions sont bien typées (typage statique, \textit{static type checking} en anglais).
L'arbre de syntaxe abstraite permet d'évaluer directement le programme ou bien de le traduire en un autre langage, plus bas niveau (en C ou en Assembleur).

L'algorithme LL(1) effectue l'étape d'analyse syntaxique pour une certaine classe de langages engendrés par des grammaires LL(1).

\section{Langages}

\begin{definition}[Alphabet]
  Un alphabet $\Sigma$ est un ensemble fini de symboles terminaux.
\end{definition}

\begin{definition}[Mot]
  Un mot $\omega$ sur un alphabet $\Sigma$ est une suite finie de
  symboles $(\omega_{n})_{n},  \forall n, \omega_{n} \in \Sigma$.
\end{definition}


\begin{definition}[Langage]
  Un langage $L$ est un ensemble de mots.
\end{definition}


\begin{example}
  Les langages réguliers (ou rationnels) sont obtenus à l'aide d'une expression rationnelle $( + ,  \cdot  ,  *)$ sur un alphabet $\Sigma$ fini.
\end{example}

\begin{remark}
  Ils correspondent aux langages reconnus par automates finis.
\end{remark}


%\begin{definition}[Grammaire]
%  Une grammaire $G$ définit un langage $L$ par rapport à un alphabet $\sum$ donné. Elle permet de déterminer si un mot défini sur $\sum$ appartient au langage $L$.
%\end{definition}


\begin{example}[Langage non régulier]
  Le langage $ \left\{ a^{n}b^{n}, n \in \Nat \right\}$ n'est pas un langage régulier.
  Cependant, il est reconnu par un automate avec une faible mémoire, ou un automate à pile.
\end{example}


\section{Grammaires hors-contexte}

%Les définitions suivantes sont issues du cours d'Analyse de données structurées de Ralf Treinen pour la L2.

\begin{definition}[Grammaire algébrique]
  Une grammaire algébrique (ou hors-contexte) est la donnée
  \begin{itemize}
    \item d'un ensemble $N$ fini de symboles non terminaux
    \item d'un ensemble $\Sigma$ fini et disjoint de $N$ de symboles terminaux
    \item d'un élément $S$ de $N$ qui correspond à l'axiome de $G$
    \item d'un ensemble $P$ fini de règles de production de la forme $A\rightarrow u$, où $A\in N$ et $u\in (\Sigma\cup N)^{*}$.
  \end{itemize}
  On note alors $G=(N, \Sigma, S, P)$.
\end{definition}

\begin{example}[Expressions arithmétiques]
  On se donne la grammaire suivante:
  \begin{minted}{text}
  S -> E
  E -> int | (E Op E)
  Op -> + | - | * | /
  \end{minted}
  Ici, \texttt{S} est l'axiome.
  Est-ce que l'expression \mintinline{text}{(int * (int + int))} appartient à la grammaire?
  Oui car on a ce que l'on appelle une \textbf{dérivation}:
  \begin{minted}{text}
    S -> E -> (E Op E) -> (int Op E) -> (int * E)
      -> (int * (E Op E))
      -> (int * (int Op E)) -> (int * (int + E))
      -> (int * (int + int))
  \end{minted}
  C'est de plus une dérivation à gauche.
  Ce mot possède d'autres dérivations qui correspondent à différents parcours de l'\textbf{arbre de dérivation}:

  \begin{center}
    \begin{tikzpicture}[
        baseline,
        font=\ttfamily,
        level distance=3em,
        text depth=.1em,
        text height=.8em,
        level 1/.style={sibling distance=1em},
        level 2/.style={sibling distance=6em},
        level 3/.style={sibling distance=2em},
        level 4/.style={sibling distance=1em},
        nodes={},-]

      % Tree structure
      \node {S}
      child { node {E}
          child { node {(} }
          child { node {E} child { node {int} } }
          child { node {Op} child { node {*} } }
          child { node {E}
              child { node {(} }
              child { node {E} child { node {int} } }
              child { node {Op} child { node {+} } }
              child { node {E} child { node {int} } }
              child { node {)} }
            }
          child { node {)} }
        };

    \end{tikzpicture}
  \end{center}

\end{example}

\begin{notation}
  Une dérivation d'un mot $w$ par la grammaire $G$ se note $S\rightarrow_G w$.
\end{notation}

\begin{definition}[Dérivation gauche et droite]
  Une dérivation gauche (respectivement droite) est une dérivation dans laquelle chaque étape dérive le symbole non terminal le plus à gauche (resp. le plus à droite).
\end{definition}

\begin{definition}[Langage engendré par une grammaire]
  Le langage engendré par une grammaire $G$ noté $\mathcal{L}(G)$ correspond à l'ensemble
  \[
    \{w \ | \ w\in\Sigma^{*}, S\rightarrow_G w\}.
  \]

\end{definition}

\begin{example}[Grammaire ambiguë]
  Considérons maintenant la grammaire suivante:

  \begin{minted}{text}
  S -> E
  E -> int | E Op E
  Op -> + | - | * | /
  \end{minted}

  Le mot \mintinline{text}{int + int * int} appartenant au langage engendré par cette grammaire possède deux arbres de dérivation différents :

  \begin{figure}[h]
    \centering
    \begin{tikzpicture}[
        baseline,
        font=\ttfamily,
        level distance=3em,
        text depth=.1em,
        text height=.8em,
        level 1/.style={sibling distance=1em},
        level 2/.style={sibling distance=4em},
        level 3/.style={sibling distance=2em},
        level 4/.style={sibling distance=1em},
        nodes={},-]

      % Tree structure
      \node {S}
      child { node {E}
          child { node {E} child { node {int} } }
          child { node {Op} child { node {+} } }
          child { node {E}
              child { node {E} child { node {int} } }
              child { node {Op} child { node {*} } }
              child { node {E} child { node {int} } }
            }
        };

    \end{tikzpicture}
    \begin{tikzpicture}[
        baseline,
        font=\ttfamily,
        level distance=3em,
        text depth=.1em,
        text height=.8em,
        level 1/.style={sibling distance=1em},
        level 2/.style={sibling distance=4em},
        level 3/.style={sibling distance=2em},
        level 4/.style={sibling distance=1em},
        nodes={},-]

      % Tree structure
      \node {S}
      child { node {E}
          child { node {E}
              child { node {E} child { node {int} } }
              child { node {Op} child { node {+} } }
              child { node {E} child { node {int} } }
            }
          child { node {Op} child { node {*} } }
          child { node {E} child { node {int} } }
        };

    \end{tikzpicture}
  \end{figure}

  C'est donc une grammaire ambiguë.
\end{example}

\begin{definition}[Grammaire non ambiguë]
  Une grammaire $G$ n'est pas ambiguë si tout mot $w\in\mathcal{L}(G)$ possède un seul arbre de dérivation.
\end{definition}


\begin{definition}[Grammaire récursive à droite]
  Une grammaire $G$ est récursive à gauche s'il existe un
  non terminal $A\in N$ pour lequel
  \[
    A \rightarrow_G A \alpha \quad \alpha\in(N \cup \Sigma^*)
  \]
\end{definition}



% \begin{definition}[Grammaire LL(k)]
%   Soit $G=(N,\Sigma, S, P)$ une grammaire et $k\in\Nat$.
%   G est LL(k) si et seulement si:

%   S'il existe deux dérivations gauches
%   \[
%     S \rightarrow^{*} uY\alpha\rightarrow u\beta\alpha\rightarrow^{*} ux
%   \]
%   \[
%     S \rightarrow^{*} uY\alpha\rightarrow u\gamma\alpha\rightarrow^{*} uy
%   \]
%   avec $Y\in N$, $u, x, y\in\Sigma^{*}$, $\alpha,\beta,\gamma\in(N\cup\Sigma)^{*}$ et $\beta\neq\gamma$ alors $x:k\neq y:k$.
% \end{definition}

% Autrement dit, une grammaire est LL(k) si, pour deux mots dérivés à partir de $uY\alpha$ et dont les arbres de dérivation sont différents, les préfixes de longueur k des mots dérivés de $Y\alpha$ sont différents.
% Une autre manière de le formuler consiste à remarquer que pour choisir quelle dérivation à partir de $Y$ il faut appliquer, on a besoin de lire les k lettres suivantes.

\section{Grammaire LL(1)}

Une grammaire est LL(1) si lorsque l'on établit l'arbre de dérivation de tout mot, on a besoin de lire le caractère suivant pour choisir quelle réécriture d'un non terminal choisir.
On a donc la définition suivante:

\begin{definition}[Grammaire LL(1)]\label{defll1}
  Une grammaire hors-contexte $G=(N,\Sigma, S, P)$ est dite LL(1) si elle vérifie la propriété suivante :

  Pour deux dérivations arbitraires
  \[
    S \rightarrow_{L} u A \gamma \rightarrow u\alpha \gamma \rightarrow_{G} uv
  \]
  et
  \[
    S \rightarrow_{L} u A \gamma \rightarrow u\alpha' \gamma \rightarrow_{G} uv'
  \]
  avec $u,v\in\Sigma^{*}, A\in N, \alpha,\gamma\in (N\cup \Sigma)^{*}$
  et l'on désigne la dérivabilité à gauche par $\rightarrow_{L}$. % TODO: ajouter référence
  Si le premier terminal de $v$ et de $v'$ sont égaux alors $\alpha=\alpha'$.

\end{definition}

\begin{remark}[Existence d'un arbre de dérivation]
  Pour $G$ LL1, $w \in L(G)$ si et seulement si il existe un arbre de dérivation de $w$.

  $(\Longrightarrow)$ $w \in L(G)$ implique qu'il existe une dérivation, donc un arbre de dérivation.

  $(\Longleftarrow)$ Par un parcours (disons préfixe) de l'arbre on trouve une dérivation (gauche) de $w$ donc $w \in L(G)$ par définition.


\end{remark}

\begin{theorem}\label{ll1nonambig}
  Une grammaire LL(1) n'est pas ambiguë.
\end{theorem}

Pour cela, on montre le lemme suivant:
\begin{lemma}\label{leftmostderivll1}
  Pour une grammaire $G$ hors-contexte LL(1) et un mot $w\in\Sigma^{*}$,
  $w\in\mathcal{L}(G)$ si et seulement si il existe une unique dérivation gauche pour $w$.
\end{lemma}


\begin{proof}

  ($\Longrightarrow$)
  Existence : Puisque $w\in\mathcal{L}(G)$, il existe un arbre de dérivation pour $w$.
  Une dérivation gauche est obtenue en effectuant un parcours en profondeur gauche.

  Unicité: Par contradiction, supposons qu'il existe au moins
  deux dérivations gauches pour le mot $w$
  \[
    S \rightarrow_{L} u A \gamma \rightarrow u \alpha \gamma \rightarrow_{L} u v = w
  \]
  et
  \[
    S \rightarrow_{L} u A \gamma \rightarrow u \alpha' \gamma \rightarrow_{L} uv' = w.
  \]
  Puisque $G$ est LL(1) et que le premier terminal de $v$ correspond au premier terminal de $v'$, on obtient $\alpha=\alpha'$.
  Par induction, on montre que les dérivations gauches pour $\alpha\gamma$ et $\alpha'\gamma$ sont égales (on les écrit sous la forme $u'A'\gamma'$
  et on applique le même raisonnement jusqu'à ce que tous les symboles non terminaux aient été réécrits en symboles terminaux).
  On conclut que les deux dérivations gauches sont égales, chaque réécriture du non terminal $A,A'$ étant unique.
  Donc il n'existe qu'une seule dérivation gauche pour le mot $w$.

  ($\Longleftarrow$) Puisqu'il existe une unique dérivation gauche pour $w$, en particulier $w\in\mathcal{L}(G)$.
\end{proof}

\begin{proof}[du théorème \ref{ll1nonambig}]
  Soit $G$ une grammaire LL(1) et $w\in\mathcal{L}(G)$.
  Le mot $w$ possède donc un arbre de dérivation. Dès que l'on modifie les nœuds de cet arbre le parcours en profondeur gauche change,
  donc la dérivation gauche de $w$ change. Or d'après le lemme \ref{leftmostderivll1},
  il existe une unique dérivation gauche pour $w$.
  Donc il n'y a qu'un seul arbre de dérivation pour $w$.
\end{proof}

%TODO: Définitions et calcul de FIRST, FOLLOW et NULLABLE

\begin{definition}[$\fst_k$]
  Soit $G=(N,\Sigma, S, P)$ une grammaire et un mot $\alpha\in(N\cup\Sigma)^{*}$. On définit la fonction $\fst_k: (N\cup\Sigma)^{*}\rightarrow 2^{\Sigma^{*}}$ par
  \[
    \fst_k(\alpha) = \{ w : k \ | \ w\in\Sigma^{*}, \alpha \rightarrow^{*} w \},
  \]
  où $w : k$ est le préfixe de longueur $k$ du mot $w$.
  C'est donc l'ensemble des préfixes de longueur k des mots terminaux que l'on peut obtenir à partir de $\alpha$.
\end{definition}

% \begin{definition}[$\follow_k$]
%   Soit $G=(N,\Sigma, S, P)$ une grammaire et $k\in\Nat$. On définit la fonction $\follow_k: N \rightarrow 2^{\Sigma^{*}}$ par
%   \[
%     \follow_k(A) = \{ w \ | \ S \rightarrow^{*} \beta A\gamma, \beta,\gamma\in(N\cup\Sigma)^{*}, w\in\fst_k(\gamma)\}.
%   \]
%   C'est l'ensemble des préfixes de longueur k des mots terminaux qui suivent un symbole non terminal A.
% \end{definition}


% \begin{theorem}[Caractérisation des grammaires LL(1)]
%   La grammaire $G=(N,\Sigma,S,P)$ est LL(1) si et seulement si pour toutes les alternatives
%   $A \rightarrow \alpha_1 | ... | \alpha_n$ :
%   \begin{itemize}[label=$\star$]
%     \item $\fst_1(\alpha_1), \dots, \fst_1(\alpha_n)$ sont deux à deux disjoints
%     \item Si $\epsilon\in \fst_1(\alpha_i)$ alors pour tous $j \neq i$ :
%           \[
%             \fst_1(\alpha_j) \cap \follow_1(A) = \emptyset.
%           \]
%   \end{itemize}
% \end{theorem}

%\begin{proof}
%  Le premier point est nécessaire car sinon la grammaire est ambiguë.
%  Le deuxième point est nécessaire également car si $\alpha_i$ se dérive en $\epsilon$, on choisit alors la dérivation suivante à appliquer en fonction de $\follow_1(A)$, ie. de l'un des caractères pouvant suivre $A$. Mais dans ce cas, si $\alpha_j$ a un $\fst_1$ qui contient un caractère de $\follow_1(A)$, il y a de nouveau une ambiguïté : on ne sait pas quelle dérivation choisir : $\alpha_i$ ou $\alpha_j$ ?
%  Reste à montrer que ces deux conditions sont suffisantes : découle de la remarque \ref{remll}, on peut expliciter qu'il n'y a pas d'autre alternative possible.
%\end{proof}
%TODO: Aborder la construction d'une table de parsing LL(1) et l'algorithme qui en découle : on choisit la réécriture à appliquer pour un non terminal en fonction du token suivant.

%Construction de la table LL(1):


\section{Parser LL(1)}

\begin{minted}{text}
  tree : arbre dont les noeuds sont étiquetés par des symboles terminaux ou non terminaux
  rule : N * ((T + N)*)
  predict : N -> T -> option rule

  update_tree : (T + N)* -> tree -> tree
  (* ajoute chaque non terminal et terminal à la feuille la plus à gauche étiquetée par un non terminal *)

  LL1: predict -> (T + N)* -> T+ -> tree -> option tree
  = λ predict α w tree ->
  match α, w with
  | ϵ, ϵ -> Some tree
  | ϵ, _ -> None
  | aα', aw' -> LL1 predict α' w' tree (* accept *)
  | aα', _ -> None
  | Nα', aw' ->
  (match predict N a with
    | None -> None
    | Some (_, α'') -> LL1 predict (α''α') w (update_tree α'' tree) (* predict *)
  )
  | Nα', _ -> None
\end{minted}

Algorithme de complexité linéaire en la longueur du mot $w$ car il n'y a qu'une réécriture
possible d'un non terminal en fonction du terminal suivant.

\section{Correction de LL(1)}

\begin{theorem}[Correction de l'algorithme LL(1)]
  Pour une grammaire $G$ hors-contexte LL(1) et un mot $w\in\Sigma^{*}$,
  \[
    w\in\mathcal{L}(G) \Longleftrightarrow \textnormal{LL(1) retourne l'arbre de dérivation de} \ w.
  \]

\end{theorem}

Pour cela, démontrons le lemme suivant et un invariant:


\begin{lemma}\label{uniqueprodrule}
  Pour $G$ LL(1) et $w\in\mathcal{L}(G)$, si
  \[
    S \rightarrow_{L} u A \gamma \rightarrow u\alpha \gamma \rightarrow_{L} uv = w
  \]
  avec $u,v\in\Sigma^{*}, A\in N, \alpha,\gamma\in (N\cup \Sigma)^{*}$, alors la connaissance de $\fst_1(v)$ assure l'existence et l'unicité de la règle de production $A\rightarrow\alpha$.
\end{lemma}

\begin{proof}
  Découle de la définition de $G$ LL(1) et de l'unicité de la dérivation gauche d'un mot $w$ de $G$ (cf. lemme \ref{leftmostderivll1}).
\end{proof}

\begin{invariant}\label{invll1}
  Si $G$ est LL(1) et $w\in\mathcal{L}(G)$ alors
  pour $u,v\in \Sigma^{*}, A\in N, \gamma\in (N\cup \Sigma)^{*}$,
  LL(1) s'appelle récursivement avec
  $uA\gamma$ et $uv$ suffixe de $w$ (possiblement égal à $w$)
  ou bien avec $v$ et $v$.

\end{invariant}

\begin{proof}
  La propriété est vraie lorsque LL(1) est appelé avec l'axiome $S$ de la grammaire et $w$.
  Montrons que l'invariant est préservé suite à un appel de LL(1)
  avec $uA\gamma$ et $uv$.

  La grammaire $G$ étant LL(1), on sait d'après le lemme \ref{leftmostderivll1}
  qu'il existe une unique dérivation gauche
  pour $w$.
  Donc on a la dérivation gauche suivante, sachant que $s\in\Sigma^{*}$ préfixe de $w$ a déjà été accepté par LL(1) :
  \[
    S \rightarrow_{L} s u A \gamma \rightarrow s u \alpha \gamma \rightarrow_{L} s u v = w.
  \]
  On distingue deux cas:
  \begin{enumerate}
    \item Soit $u\neq\epsilon$ et le cas $a\alpha', aw'$ de LL(1) est vérifié.
          On appelle alors LL(1) avec $u' A \gamma$ et $u'v$ où $u=\fst_1(u)u'$.

    \item Soit $u=\epsilon$ et le cas $N\alpha', aw'$ est vérifié.
          Alors on appelle la fonction de prédiction avec $A$ et $\fst_1(v)$.
          Donc d'après le lemme \ref{uniqueprodrule},
          il existe une unique règle de production $A\rightarrow\alpha$.
          On discerne trois possibilités, selon que $\alpha,\gamma\in\Sigma^*$ ou non.
          Si $\alpha,\gamma\in\Sigma^*$ alors LL(1) est appelé avec $\alpha\gamma=v$ et $v$.
          Si $\alpha\in\Sigma^*$ alors LL(1) est appelé avec $\alpha\gamma$ et $v$, $\gamma$ est de la
          forme $u' A' \gamma', u'\in\Sigma^{*}, A'\in N, \gamma'\in (N \cup \Sigma)^*$ et $\alpha u'$ est un préfixe de $v$. 
          Sinon, le mot $w$ ayant une unique dérivation gauche, $\alpha$ est
          de la forme $u' A' \gamma', u'\in\Sigma^{*}, A'\in N, \gamma'\in (N \cup \Sigma)^*$
          et l'on appelle bien LL(1) avec $u' A' \gamma'\gamma$ et $v$ suffixe de $w$, $u'$ étant un préfixe de $v$.
  \end{enumerate}

  L'invariant est également préservé lorsque LL(1) est appelé avec $v$ et $v$ préfixe de $w$: si $v\neq\epsilon$
  alors on retrouve le cas 1. traité précédemment. Si $v=\epsilon$ alors l'algorithme termine.
\end{proof}


Nous disposons maintenant de tous les outils pour démontrer la correction de l'algorithme LL(1):
\begin{proof}[Correction de l'algorithme LL(1)]

  ($\Longleftarrow$) Un arbre de dérivation d'un mot correspond à une dérivation de ce mot.

  ($\Longrightarrow$) On sait que $G$ est LL(1) et que $w\in\mathcal{L}(G)$ donc il existe une unique dérivation gauche
  pour $w$ d'après le lemme \ref{leftmostderivll1}.
  D'après l'invariant \ref{invll1}, la longueur finie de la dérivation gauche de $w$
  garantit la terminaison de LL(1) avec le cas $\epsilon, \epsilon$.
  On peut se convaincre que LL(1) construit l'arbre de dérivation de $w$ et le retourne.
\end{proof}

%\printbibliography

\end{document}

